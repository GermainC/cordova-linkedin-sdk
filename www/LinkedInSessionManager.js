var exec = require('cordova/exec');
var _ = require('cordova-plugin-linkedin.underscorejs');
var Q = require('cordova-plugin-linkedin.Q');

function SessionManager() {
}

SessionManager.prototype._promisedExec = function (method, commandArgs, preProcessors) {
	var self = this;
	commandArgs = _.isArray(commandArgs) ? commandArgs : [];
	preProcessors = _.isArray(preProcessors) ? preProcessors : [];
	preProcessors = _.filter(preProcessors, function(preProcessor) {
		return _.isFunction(preProcessor);
	});

	var d = Q.defer();

	var resolveWrap = function(pluginResult) {
		self._preProcessorExecutor(d.resolve, pluginResult, preProcessors);
	};

	exec(resolveWrap, d.reject, "SessionManager", method, commandArgs);

	return d.promise;
};

SessionManager.prototype._preProcessorExecutor = function (resolve, pluginResult, preProcessors) {
	_.each(preProcessors, function (preProcessor) {
		pluginResult = preProcessor(pluginResult);
	});
	resolve(pluginResult);
};

/**
 * Authenticate with LinkedIn user account.
 *
 * List of the possible values for authentication error code : 
 * NONE, INVALID_REQUEST, NETWORK_UNAVAILABLE, USER_CANCELLED, UNKNOWN_ERROR, SERVER_ERROR, LINKEDIN_APP_NOT_FOUND, NOT_AUTHENTICATED
 * 
 * @param  {Object} args { permissions: <Array> }
 * @return {Q.Promise} 
 *         - Data when the promise is resolved : { accessTokenValue: <String>, expiresOn: <int> }
 *         - Data when the promise is rejected : { errorCode: <int>, errorMessage: <Object> }
 */
SessionManager.prototype.authenticate = function(args) {
	return this._promisedExec('authenticate', [args], []);
};

/**
 * Apply get request on LinkedIn API
 * 
 * @param  {Object} args { url: <String> }
 * @return {Q.Promise} The promise with command result : { statusCode: <int>, data: <Object> }
 */
SessionManager.prototype.get = function(args) {
	return this._promisedExec('getRequest', [args], []);
};

/**
 * Apply post request on LinkedIn API
 * 
 * @param  {Object} args { url: <String>, body: <Object> }
 * @return {Q.Promise} The promise with command result : { statusCode: <int>, data: <Object> }
 */
SessionManager.prototype.post = function(args) {
	return this._promisedExec('postRequest', [args], []);
};

/**
 * Apply put request on LinkedIn API
 * 
 * @param  {Object} args { url: <String>, body: <Object> }
 * @return {Q.Promise} The promise with command result : { statusCode: <int>, data: <Object> }
 */
SessionManager.prototype.put = function(args) {
	return this._promisedExec('putRequest', [args], []);
};

/**
 * Apply delete request on LinkedIn API
 * 
 * @param  {Object} args { url: <String> }
 * @return {Q.Promise} The promise with command result : { statusCode: <int>, data: <Object> }
 */
SessionManager.prototype.delete = function(args) {
	return this._promisedExec('deleteRequest', [args], []);
};

var sessionManager = new SessionManager();

module.exports.linkedin = sessionManager;

