package com.lesgeorges.cordova.linkedinsdk;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.linkedin.platform.APIHelper;
import com.linkedin.platform.AccessToken;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.InvalidKeyException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by florian on 06/12/15.
 */
public class LiSessionManagerCDVPlugin extends CordovaPlugin {

    public static String TAG = LiSessionManagerCDVPlugin.class.getName();

    public static String ACTION_AUTHENTICATE = "authenticate";
    public static String ACTION_GET_REQUEST = "getRequest";
    public static String ACTION_POST_REQUEST = "postRequest";
    public static String ACTION_PUT_REQUEST = "putRequest";
    public static String ACTION_DELETE_REQUEST = "deleteRequest";


    public static String ACTION_INIT_PLUGIN_RESULT_TOKEN_EXPIRES_ON = "expiresOn";
    public static String ACTION_INIT_PLUGIN_RESULT_TOKEN_VALUE = "accessTokenValue";

    private enum PermissionType {
        r_basicprofile,
        r_fullprofile,
        r_emailaddress,
        r_contactinfo,
        rw_company_admin,
        w_share
    };

    private static Scope.LIPermission getPermission(String permissionStr) {
        switch (PermissionType.valueOf(permissionStr)) {
            case r_basicprofile:
                return Scope.R_BASICPROFILE;
            case r_fullprofile:
                return Scope.R_FULLPROFILE;
            case r_emailaddress:
                return Scope.R_EMAILADDRESS;
            case r_contactinfo:
                return Scope.R_CONTACTINFO;
            case rw_company_admin:
                return Scope.RW_COMPANY_ADMIN;
            case w_share:
                return Scope.W_SHARE;
            default:
                return null;
        }
    }

    // Build the list of member permissions our LinkedIn session requires
    public static Scope buildScope(String... permissions) {
        Set<Scope.LIPermission> liPermissions = new HashSet<Scope.LIPermission>();

        if (liPermissions == null) {
            return Scope.build();
        }

        for (String perm : permissions) {
            liPermissions.add(LiSessionManagerCDVPlugin.getPermission(perm));
        }

        return Scope.build(liPermissions.toArray(new Scope.LIPermission[]{}));
    }

    public LiSessionManagerCDVPlugin() {
    }

    private void authenticate(final JSONObject arguments, final CallbackContext callbackContext) {
        cordova.setActivityResultCallback(this);

        // Store a reference to the current activity
        final Activity thisActivity = cordova.getActivity();
        final LISessionManager sm = LISessionManager.getInstance(thisActivity.getApplicationContext());

        try {
            Scope scope = buildScope(parsePermissions(arguments));

            sm.init(thisActivity, scope, new AuthListener() {
                @Override
                public void onAuthSuccess() {
                    try {
                        Log.i(TAG, "authenticate: auth success");

                        if (callbackContext == null) {
                            return;
                        }

                        // Transform access token to json object
                        JSONObject data = new JSONObject(sm.getSession().getAccessToken().toString());

                        // Notify callback context with the access token
                        PluginResult result = new PluginResult(PluginResult.Status.OK, data);
                        result.setKeepCallback(true);

                        callbackContext.sendPluginResult(result);

                    } catch(JSONException ex) {
                        Log.e(TAG, "init exception : " + ex.getCause());

                        PluginResult result = new PluginResult(PluginResult.Status.ERROR, ex.getMessage());
                        result.setKeepCallback(true);

                        callbackContext.sendPluginResult(result);
                    }
                }

                @Override
                public void onAuthError(LIAuthError error) {
                    Log.e(TAG, "authenticate: auth error: " + error.toString());

                    if (callbackContext == null) {
                        return;
                    }

                    JSONObject data = null;
                    try {
                        data = new JSONObject(error.toString());
                    } catch (JSONException ex) {
                        Log.e(TAG, "json error: " + ex.getCause());
                    }

                    PluginResult result = new PluginResult(PluginResult.Status.ERROR, data);
                    result.setKeepCallback(true);

                    callbackContext.sendPluginResult(result);
                }
            }, true);

        } catch (JSONException ex) {
            Log.e(TAG, "authenticate: json error: " + ex.getCause());

            PluginResult result = new PluginResult(PluginResult.Status.ERROR,
                    "Json parse error: " + ex.getMessage());
            result.setKeepCallback(true);

            callbackContext.sendPluginResult(result);

        } catch (InvalidKeyException ex) {
            Log.e(TAG, "authenticate: function argument error: " + ex.getCause());

            PluginResult result = new PluginResult(PluginResult.Status.ERROR,
                    ": " + ex.getMessage());
            result.setKeepCallback(true);

            callbackContext.sendPluginResult(result);
        }
    }

    private void deleteRequest(final JSONObject arguments, final CallbackContext callbackContext) {
        doApiRequest(arguments, callbackContext, new CDVRequestHandler() {
            @Override
            public void doRequest(Activity activity, ApiRequest request, ApiListener apiListener) {
                APIHelper.getInstance(activity.getApplicationContext())
                        .deleteRequest(activity, request.getUrl(), apiListener);
            }
        });
    }

    private void putRequest(final JSONObject arguments, final CallbackContext callbackContext) {
        doApiRequest(arguments, callbackContext, new CDVRequestHandler() {
            @Override
            public void doRequest(Activity activity, ApiRequest request, ApiListener apiListener) {
                APIHelper.getInstance(activity.getApplicationContext())
                        .putRequest(activity, request.getUrl(), request.getBody(), apiListener);
            }
        });
    }

    private void postRequest(final JSONObject arguments, final CallbackContext callbackContext) {
        doApiRequest(arguments, callbackContext, new CDVRequestHandler() {
            @Override
            public void doRequest(Activity activity, ApiRequest request, ApiListener apiListener) {
                APIHelper.getInstance(activity.getApplicationContext())
                        .postRequest(activity, request.getUrl(), request.getBody(), apiListener);
            }
        });
    }

    private void getRequest(final JSONObject arguments, final CallbackContext callbackContext) {
        doApiRequest(arguments, callbackContext, new CDVRequestHandler() {
            @Override
            public void doRequest(Activity activity, ApiRequest request, ApiListener apiListener) {
                APIHelper.getInstance(activity.getApplicationContext())
                        .getRequest(activity, request.getUrl(), apiListener);
            }
        });
    }

    private void doApiRequest(final JSONObject arguments, final CallbackContext callbackContext, CDVRequestHandler requestHandler) {
        final Activity thisActivity = cordova.getActivity();

        try {
            ApiRequest request = parseApiRequest(arguments);

            requestHandler.doRequest(thisActivity, request, new CDVApiListener(callbackContext));

        } catch (JSONException ex) {
            Log.e(TAG, "api request: json error: " + ex.getCause());

            PluginResult result = new PluginResult(PluginResult.Status.ERROR,
                    "Json parse error: " + ex.getMessage());
            result.setKeepCallback(true);

            callbackContext.sendPluginResult(result);
        } catch (InvalidKeyException ex) {
            Log.e(TAG, "api request: function argument error: " + ex.getCause());

            PluginResult result = new PluginResult(PluginResult.Status.ERROR,
                    ": " + ex.getMessage());
            result.setKeepCallback(true);

            callbackContext.sendPluginResult(result);
        }
    }

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals(ACTION_AUTHENTICATE)) {
            authenticate(args.optJSONObject(0), callbackContext);
        } else if (action.equals(ACTION_GET_REQUEST)) {
            getRequest(args.optJSONObject(0), callbackContext);
        } else if (action.equals(ACTION_POST_REQUEST)) {
            postRequest(args.optJSONObject(0), callbackContext);
        } else if (action.equals(ACTION_PUT_REQUEST)) {
            putRequest(args.optJSONObject(0), callbackContext);
        } else if (action.equals(ACTION_DELETE_REQUEST)) {
            deleteRequest(args.optJSONObject(0), callbackContext);
        } else {
            return super.execute(action, args, callbackContext);
        }

        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        Log.i(TAG, "activity result: " + (intent != null ? intent.toString() : "no intent"));

        final Activity thisActivity = cordova.getActivity();

        // Forward authentication result intent to LinkedInSDK
        LISessionManager.getInstance(thisActivity.getApplicationContext())
                .onActivityResult(thisActivity, requestCode, resultCode, intent);

        super.onActivityResult(requestCode, resultCode, intent);
    }

    /////////// SERIALISATION /////////////////////

    public String[] parsePermissions(JSONObject json) throws JSONException, InvalidKeyException {
        if (!json.has("permissions")) {
            throw new InvalidKeyException("'permissions' parameter is missing.");
        }

        JSONArray jsonArray = (JSONArray)json.getJSONArray("permissions");

        if (jsonArray != null) {
            String[] list = new String[jsonArray.length()];
            int len = jsonArray.length();
            for (int i=0;i<len;i++) {
                list[i] = jsonArray.get(i).toString();
            }
            return list;
        } else {
            return new String[]{};
        }
    }

    public ApiRequest parseApiRequest(JSONObject json) throws JSONException, InvalidKeyException {
        if (!json.has("url")) {
            throw new InvalidKeyException("'url' parameter is missing.");
        }

        ApiRequest request = new ApiRequest();

        request.setUrl(json.getString("url"));

        if (json.has("body")) {
            request.setBody(json.getJSONObject("body"));
        }

        return request;
    }

    private class ApiRequest {

        private String url;
        private JSONObject body;

        public ApiRequest() {}

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public JSONObject getBody() {
            return body;
        }

        public void setBody(JSONObject body) {
            this.body = body;
        }
    }

    // Api request Listener
    class CDVApiListener implements ApiListener {

        private CallbackContext mCallbackContext;

        public CDVApiListener(CallbackContext callbackContext) {
            mCallbackContext = callbackContext;
        }

        @Override
        public void onApiSuccess(ApiResponse apiResponse) {
            Log.i(TAG, "api get request: success");

            try {
                JSONObject data = new JSONObject();
                data.put("statusCode", apiResponse.getStatusCode());
                data.put("data", apiResponse.getResponseDataAsJson());

                PluginResult result = new PluginResult(PluginResult.Status.OK, data);
                result.setKeepCallback(true);

                mCallbackContext.sendPluginResult(result);

            } catch (JSONException ex) {
                Log.e(TAG, "json error: " + ex.getCause());
            }
        }

        @Override
        public void onApiError(LIApiError liApiError) {
            Log.e(TAG, "api get request: error type " + liApiError.getErrorType() +
                    ", status code = " + liApiError.getHttpStatusCode() +
                    ", details = " + liApiError.toString());

            try {
                JSONObject data = new JSONObject();
                data.put("statusCode", liApiError.getHttpStatusCode());
                data.put("data", new JSONObject(liApiError.getApiErrorResponse().toString()));

                PluginResult result = new PluginResult(PluginResult.Status.ERROR, data);
                result.setKeepCallback(true);

                mCallbackContext.sendPluginResult(result);

            } catch (JSONException ex) {
                Log.e(TAG, "json error: " + ex.getCause());
            }
        }
    }

    private interface CDVRequestHandler {
        void doRequest(Activity activity, ApiRequest request, ApiListener apiListener);
    }
}
