package com.lesgeorges.cordova.linkedinsdk;

import com.linkedin.platform.utils.Scope;

import junit.framework.TestCase;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;

/**
 * Created by florian on 10/12/15.
 */
public class LiSessionManagerCDVPluginTestCase extends TestCase {

    public void testBuildScope() {
        Scope scope = LiSessionManagerCDVPlugin.buildScope("r_basicprofile");
        assertEquals("r_basicprofile", scope.toString());

        scope = LiSessionManagerCDVPlugin.buildScope("r_basicprofile", "r_emailaddress");
        assertEquals("r_basicprofile r_emailaddress", scope.toString());
    }

    public void testParsePermissions() throws Exception {
        JSONObject jsonObj = new JSONObject("{\"permissions\":[\"r_basicprofile\",\"r_emailaddress\"]}");
        LiSessionManagerCDVPlugin plugin = new LiSessionManagerCDVPlugin();
        String[] permissions = plugin.parsePermissions(jsonObj);
        assertEquals("r_basicprofile,r_emailaddress", permissions.toString());
    }
}
