//
//  linkedinsdk.h
//  linkedinsdk
//
//  Created by florian mhun on 10/12/2015.
//  Copyright © 2015 Florian Mhun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <linkedin-sdk/LISDK.h>
#import <Cordova/CDV.h>

const int LI_SESSION_MANAGER_CDV_PLUGIN_INPUT_PARSE_ERROR = 100;

@interface LiSessionManagerCDVPlugin : CDVPlugin {
}

@property (strong, nonatomic) LISDKSession *session;

- (void) authenticate:(CDVInvokedUrlCommand *)command;
- (void) getRequest:(CDVInvokedUrlCommand *)command;
- (void) postRequest:(CDVInvokedUrlCommand *)command;
- (void) putRequest:(CDVInvokedUrlCommand *)command;
- (void) deleteRequest:(CDVInvokedUrlCommand *)command;

#pragma mark - Util_Methods


@end