//
//  linkedinsdk.m
//  linkedinsdk
//
//  Created by florian mhun on 10/12/2015.
//  Copyright © 2015 Florian Mhun. All rights reserved.
//

#import "LiSessionManagerCDVPlugin.h"

@implementation LiSessionManagerCDVPlugin

- (void)pluginInitialize {
    NSLog(@"Cordova LinkedIn Plugin");
    
    // Add notification listener for handleOpenURL
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(openURL:)
                                                 name:CDVPluginHandleOpenURLNotification object:nil];
    [super pluginInitialize];
}

- (void)openURL:(NSNotification *)notification {
    NSURL *url = [notification object];
    
    if (![url isKindOfClass:[NSURL class]]) {
        return;
    }
    
    NSLog(@"handle url: %@", url);
    
    if ([LISDKCallbackHandler shouldHandleUrl:url]) {
        [LISDKCallbackHandler application:nil openURL:url sourceApplication:@"com.linkedin.LinkedIn" annotation:nil];
    }
}

- (NSError*) parseErrorWithDescription:(NSString*) description {
    return [self errorWithCode:LI_SESSION_MANAGER_CDV_PLUGIN_INPUT_PARSE_ERROR andDescription:description];
}


- (NSError*) errorWithCode: (int)code andDescription:(NSString*) description {
    
    NSMutableDictionary* details;
    if (description != nil) {
        details = [NSMutableDictionary dictionary];
        [details setValue:description forKey:NSLocalizedDescriptionKey];
    }
    
    return [[NSError alloc] initWithDomain:@"LiSessionManagerCDVPlugin" code:code userInfo:details];
}

- (NSDictionary*) parseCommandArgsWithUrl: (CDVInvokedUrlCommand *)command returningError:(out NSError **)error {
    NSDictionary* args = [command.arguments objectAtIndex:0];
    
    NSString* url = [args objectForKey:@"url"];
    if (url == nil) {
        *error = [self parseErrorWithDescription:@"'url' is missing, cannot parse argument."];
        return nil;
    }
    
    return args;
}

- (NSDictionary*) parseCommandArgsWithPermissions: (CDVInvokedUrlCommand *)command returningError:(out NSError **)error {
    NSDictionary* args = [command.arguments objectAtIndex:0];
    
    NSArray* permissions = [args objectForKey:@"permissions"];
    if (permissions == nil) {
        *error = [self parseErrorWithDescription:@"'permissions' is missing, cannot parse argument."];
        return nil;
    }
    
    return args;
}

- (NSDictionary*) parseCommandArgsWithUrlAndBody: (CDVInvokedUrlCommand *)command returningError:(out NSError **)error {
    NSDictionary* args = [command.arguments objectAtIndex:0];
    
    NSString* url = [args objectForKey:@"url"];
    NSString* body = [args objectForKey:@"body"];
    if (url == nil) {
        *error = [self parseErrorWithDescription:@"'url' is missing, cannot parse argument."];
        return nil;
    } else if (body == nil) {
        *error = [self parseErrorWithDescription:@"'body' is missing, cannot parse argument."];
        return nil;
    }
    
    return args;
}

- (void) sendPluginCommand: (CDVInvokedUrlCommand *)command resultingError:(NSError*)error {
    CDVPluginResult* pluginResult;
    if (error != nil) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:error.userInfo];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Unknown error."];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (BOOL) checkSessionIsValid: (out NSError **)error {
    if ([LISDKSessionManager hasValidSession] == NO) {
        *error = [self parseErrorWithDescription:@"session is not valid, try to request a new access token"];
        return NO;
    } else {
        return YES;
    }
}

- (NSMutableDictionary*) createDictFromAPIResponse:(LISDKAPIResponse*)response {
    NSMutableDictionary* dict = [NSMutableDictionary new];
    
    NSError *jsonError;
    NSDictionary *responseDataAsDict = [NSJSONSerialization JSONObjectWithData:[response.data dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&jsonError];
    
    [dict setObject:[NSNumber numberWithInt:response.statusCode] forKey:@"statusCode"];
    [dict setObject:responseDataAsDict forKey:@"data"];
    
    return dict;
}

- (void) authenticate:(CDVInvokedUrlCommand *)command {
    NSLog(@"%s","authenticate");
    
    NSError *error;
    NSDictionary* args = [self parseCommandArgsWithPermissions: command returningError:&error];
    
    // Send plugin error and cancel method if error
    if (args == nil) {
        [self sendPluginCommand:command resultingError:error];
        return;
    }
    
    NSArray *permissions = [args objectForKey:@"permissions"];
    
    [LISDKSessionManager createSessionWithAuth:permissions
                                         state:nil
                        showGoToAppStoreDialog:YES
                                  successBlock:^(NSString *returnState) {
                                      NSLog(@"%s", "auth success");
                                      LISDKAccessToken *accessToken = [[LISDKSessionManager sharedInstance] session].accessToken;
                                      
                                      NSMutableDictionary* dict = [NSMutableDictionary new];
                                      
                                      [dict setObject:accessToken.accessTokenValue forKey:@"accessTokenValue"];
                                      [dict setObject:[NSNumber numberWithInt:[accessToken.expiration timeIntervalSinceNow]] forKey:@"expiresOn"];
                                      
                                      //NSLog(@"%s %@", "auth token: ", accessToken);
                                      
                                      CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsDictionary:dict];
                                      [pluginResult setKeepCallbackAsBool:YES];
                                      
                                      [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                                  }
                                    errorBlock: ^(NSError *error) {
                                        NSLog(@"%s %@", "auth error: ", error);
                                        
                                        NSMutableDictionary* dict = [NSMutableDictionary new];
                                        [dict setObject:[error.userInfo objectForKey:@"errorInfo"] forKey:@"errorCode"];
                                        [dict setObject:[error.userInfo objectForKey:@"errorDescription"] forKey:@"errorMessage"];
                                        
                                        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsDictionary: dict];
                                        [pluginResult setKeepCallbackAsBool:YES];
                                        
                                        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                                    }
     ];
}

- (void) handle:(CDVInvokedUrlCommand *)command apiError:(LISDKAPIError *)apiError {
    NSLog(@"%s %@", "api request error: ", apiError);
    
    NSMutableDictionary* dict = [self createDictFromAPIResponse: apiError.errorResponse];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_ERROR messageAsDictionary:dict];
    [pluginResult setKeepCallbackAsBool:YES];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) handle:(CDVInvokedUrlCommand *)command apiSuccess:(LISDKAPIResponse *)response {
    NSLog(@"%s %@", "api request sucess: ", response);
    
    NSMutableDictionary* dict = [self createDictFromAPIResponse: response];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus: CDVCommandStatus_OK messageAsDictionary:dict];
    [pluginResult setKeepCallbackAsBool:YES];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) getRequest:(CDVInvokedUrlCommand *)command {
    NSError *error;
    NSDictionary* args = [self parseCommandArgsWithUrl: command returningError:&error];
    
    // Send plugin error and cancel method if error
    if (args == nil || [self checkSessionIsValid:&error] == NO) {
        [self sendPluginCommand:command resultingError:error];
        return;
    }
    
    [[LISDKAPIHelper sharedInstance]
     getRequest:[args objectForKey:@"url"]
     success:^(LISDKAPIResponse *response) {
         [self handle:command apiSuccess: response];
     }
     error:^(LISDKAPIError *apiError) {
         [self handle:command apiError: apiError];
     }];
}

- (void) postRequest:(CDVInvokedUrlCommand *)command {
    NSError *error;
    NSDictionary* args = [self parseCommandArgsWithUrlAndBody: command returningError:&error];
    
    // Send plugin error and cancel method if error
    if (args == nil || [self checkSessionIsValid:&error] == NO) {
        [self sendPluginCommand:command resultingError:error];
        return;
    }
    
    [[LISDKAPIHelper sharedInstance]
     postRequest:[args objectForKey:@"url"]
     body:[args objectForKey:@"body"]
     success:^(LISDKAPIResponse *response) {
         [self handle:command apiSuccess: response];
     }
     error:^(LISDKAPIError *apiError) {
         [self handle:command apiError: apiError];
     }];
}

- (void) putRequest:(CDVInvokedUrlCommand *)command {
    NSError *error;
    NSDictionary* args = [self parseCommandArgsWithUrlAndBody: command returningError:&error];
    
    // Send plugin error and cancel method if error
    if (args == nil || [self checkSessionIsValid:&error] == NO) {
        [self sendPluginCommand:command resultingError:error];
        return;
    }
    
    [[LISDKAPIHelper sharedInstance]
     putRequest:[args objectForKey:@"url"]
     body:[args objectForKey:@"body"]
     success:^(LISDKAPIResponse *response) {
         [self handle:command apiSuccess: response];
     }
     error:^(LISDKAPIError *apiError) {
         [self handle:command apiError: apiError];
     }];
}

- (void) deleteRequest:(CDVInvokedUrlCommand *)command {
    NSError *error;
    NSDictionary* args = [self parseCommandArgsWithUrl: command returningError:&error];
    
    // Send plugin error and cancel method if error
    if (args == nil || [self checkSessionIsValid:&error] == NO) {
        [self sendPluginCommand:command resultingError:error];
        return;
    }
    
    [[LISDKAPIHelper sharedInstance]
     deleteRequest:[args objectForKey:@"url"]
     success:^(LISDKAPIResponse *response) {
         [self handle:command apiSuccess: response];
     }
     error:^(LISDKAPIError *apiError) {
         [self handle:command apiError: apiError];
     }];
}

@end
