#Installation

##Setup LinkedIn application

First you must initilize an app in the linkedin developer console.
iOS guide : https://developer.linkedin.com/docs/ios-sdk.
Android guide : https://developer.linkedin.com/docs/android-sdk

##Install the plugin

Add the plugin to your cordova application from the cordova plugin registery `cordova plugin add cordova-linkedin-sdk --variable APP_ID="your app id"`

or install it from sources if you want to point to a specific git revison :

`cordova plugin add https://bitbucket.org/floomoon/cordova-linkedin-sdk --variable APP_ID="your app id"`

Do not forget to add specify your APP_ID when installing the plugin.

#Native LinkedIn library

This plugin use the following native LinkedIn library versions :
- Android : 1.1.4-release (November 2, 2015)
- iOS : 1.0.7-release (January 19th, 2016)

#Plugin development

##Build native LinkedIn library for iOS

Download iOS library source code from the linkedin developer website : https://developer.linkedin.com/downloads

Copy `license.txt` and `linkedin-sdk.framework` files from the downloaded release package and paste them in the `src/ios` folder of the cordova-linkedin-sdk plugin sources.